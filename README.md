# md2term

## Tipos de markdown

### Titulação
 # TÍTULO h1
 ## TÍTULO h2
 ### TÍTULO h3
 #### TÍTULO h4
 ##### TÍTULO h5
 ###### TÍTULO h6

### Ênfase
 Negrito      **negrito** ou __negrito__
 Itálico      _itálico_
 Tachado      ~~tachado~~
 Sublinhado   <u>sublinhado</u>
 Link         [link](href)
 Citação      > citação

### Lista de itens
 * item de lista não ordenada
 * item de lista não ordenada

 1. item de lista ordenada
 2. item de lista ordenada


### Código
 Código em linha `código em linha`
 Código multi linha
```linguagem
Código
```

### Tabela
Alinhado a esquerda | Centralizado | Alinhado a direita
:--------- | :------: | -------:
Valor | Valor | Valor
